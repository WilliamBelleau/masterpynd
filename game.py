import random
import sys
from termcolor import colored

import color


class Game:
    def __init__(self):
        """Things needed for the game"""
        self.good_pegs = []
        self.hint_pegs = []
        self.guessed_pegs = []

    def reset(self):
        self.good_pegs = []
        self.guessed_pegs = []
        self.hint_pegs = []

    def login(self):
        """Invites the player to login."""
        gamertag = input(colored('Enter your Gamertag :', 'grey', attrs=['underline']))
        print(colored('\nNew game started ! Player {}, try guessing the 4 hidden pegs.\n'.format(gamertag),
                      'grey', attrs=['bold']))
        print(colored("Here's the list of color and the number they're associated with "
                      "from which you can choose from:\n", 'grey', attrs=['bold']))
        print(colored('    Red = 1', 'red', attrs=['bold']))
        print(colored('   Blue = 2', 'blue', attrs=['bold']))
        print(colored('  Green = 3', 'green', attrs=['bold']))
        print(colored(' Yellow = 4', 'yellow', attrs=['bold']))
        print(colored('Magenta = 5', 'magenta', attrs=['bold']))
        print(colored('  White = 6\n', 'white', attrs=['bold']))
        text_redpeg = colored("------- Good color & Good position = ", 'grey', attrs=['bold'])
        print(text_redpeg, end='')
        print(colored('Red peg', 'red', attrs=['bold', 'reverse']), end='')
        print(" --------\n")
        text_whitepeg = colored("------ Good color & Wrong position = ", 'grey', attrs=['bold'])
        print(text_whitepeg, end='')
        print(colored('White peg', 'white', attrs=['bold', 'reverse']), end='')
        print(" ------")

    def next_player_turn(self):
        """The guess of the player during a turn"""

        guess = input(colored('\nSelect 4 pegs by typing them this way : 1234\n\n'
                              'Enter your guess ''here :', 'grey', attrs=['bold']))
        if len(guess) == 4 and guess.isdigit():
            for peg in guess:
                self.guessed_pegs.append(int(peg))
            print(colored('Your guess is {}'.format(guess), 'cyan', attrs=['bold']))

    def generate_solution(self):
        """Generates the list of good pegs to be guessed by the player"""
        max_good_pegs = 4
        peg_color_number = list(map(int, color.Color))
        random.shuffle(peg_color_number)
        while len(peg_color_number) > max_good_pegs:
            peg_color_number.pop(random.randrange(len(peg_color_number)))
        self.good_pegs = peg_color_number
        print(self.good_pegs, '<-- solution debug only')  # only there to help develop

    def analysis(self):
        """Validates the player guess in order for the computer to give him hints (black_peg, white_peg)."""
        red_peg = 0
        white_peg = 0
        for i, guessed_peg in enumerate(self.guessed_pegs):
            for j, good_peg in enumerate(self.good_pegs):
                if guessed_peg == good_peg:
                    if i == j:
                        red_peg += 1
                    else:
                        white_peg += 1
        print(colored('{} red peg(s)'.format(red_peg), 'red', attrs=['bold', 'reverse']))
        print(colored('{} white peg(s)'.format(white_peg), 'white', attrs=['bold', 'reverse']))

    def end_game(self):
        """End-game message"""
        print(colored('You won !', 'cyan', attrs=['bold', 'reverse']))
        return None

    def play(self):
        """Plays the game"""
        self.reset()
        self.login()
        self.generate_solution()
        while self.good_pegs != self.guessed_pegs:
            self.guessed_pegs.clear()
            self.next_player_turn()
            self.analysis()
        else:
            return self.end_game()
