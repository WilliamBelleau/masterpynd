import random
from enum import Enum, IntEnum


class Color(IntEnum):
    red = 1
    blue = 2
    green = 3
    yellow = 4
    purple = 5
    white = 6
